//
//  MidiMessage.h
//  CommandLineTool
//
//  Created by Duncan Whale on 10/2/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef H_MIDIMESSAGE
#define H_MIDIMESSAGE

class MidiMessage
{
public:
    // Default constructor.
    MidiMessage();
    
    // Second constructor.
    MidiMessage(int initialNote, int initialVelocity, int initialChannel);
    
    // Destructor.
    ~MidiMessage();
    
    // Sets the MIDI note number of the message.
    void setNoteNumber (int newNoteNumber);
    
    // Sets the velocity of the message.
    void setVelocity (int newVelocity);
    
    // Sets the channel of the message.
    void setChannel (int newChannel);
    
    // Returns the note number of the message.
    int getNoteNumber() const;
    
    // Returns the MIDI note as a frequency.
    float getMidiNoteInHertz() const;
    
    // Returns the velocity of the message.
    int getVelocity() const;
    
    // Returns the amplitude of the message.
    float getFloatVelocity() const;
    
    // Returns the channel of the message.
    int getChannel() const;
    
private:
    int note;
    int velocity;
    int channel;
};


#endif // H_MIDIMESSAGE
