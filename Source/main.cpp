//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.h"

int main (int argc, const char* argv[])
{
    int userVal;
    MidiMessage note(69, 100, 3);
    

    // Print default values.
    std::cout << "The default note is " << note.getNoteNumber() << " and the default frequency is " << note.getMidiNoteInHertz() << ".\n";
    std::cout << "The default velocity is " << note.getVelocity() << " and the default amplitude is " << note.getFloatVelocity() << ".\n";
    
    std::cout << "The default channel is " << note.getChannel() << ".\n\n";
    
    // Get data from user.
    std::cout << "Type in a MIDI note number.\n";
    std::cin >> userVal;
    note.setNoteNumber(userVal);
    
    std::cout << "Type in a velocity.\n";
    std::cin >> userVal;
    note.setVelocity(userVal);
    
    std::cout << "Type in a channel number.\n";
    std::cin >> userVal;
    note.setChannel(userVal);
    
    
    // Print new values.
    std::cout << "The new note is " << note.getNoteNumber() << " and the new frequency is " << note.getMidiNoteInHertz() << ".\n";
    std::cout << "The new velocity is " << note.getVelocity() << " and the new amplitude is " << note.getFloatVelocity() << ".\n";
    std::cout << "The new channel is " << note.getChannel() << ".\n";
    
    return 0;
}