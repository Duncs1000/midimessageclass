//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Duncan Whale on 10/2/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.h"
#include <iostream>
#include <cmath>

//  Default constructor.
MidiMessage::MidiMessage()
{
    note = 60;
    velocity = 127;
    channel = 0;
}

// Second constructor.
MidiMessage::MidiMessage(int initialNote, int initialVelocity, int initialChannel)
{
    note = initialNote;
    velocity = initialVelocity;
    channel = initialChannel;
}

// Destructor.
MidiMessage::~MidiMessage()
{
    
}

// Sets the MIDI note number of the message.
void MidiMessage::setNoteNumber(int newNoteNumber)
{
    if (newNoteNumber >= 0 && newNoteNumber < 128)
        note = newNoteNumber;
    else
        std::cout << "Error. Invalid note.\n";
}

// Sets the velocity of the message.
void MidiMessage::setVelocity(int newVelocity)
{
    if (newVelocity >= 0 && newVelocity < 128)
        velocity = newVelocity;
    else
        std::cout << "Error. Invalid velocity.\n";
}

// Sets the channel of the message.
void MidiMessage::setChannel(int newChannel)
{
    if (newChannel >= 0 && newChannel < 16)
        channel = newChannel;
    else
        std::cout << "Error. Invalid channel.\n";
}

// Returns the note number of the message.
int MidiMessage::getNoteNumber() const
{
    return note;
}

// Returns the MIDI note as a frequency.
float MidiMessage::getMidiNoteInHertz() const
{
    return 440 * pow(2, (note - 69) / 12.0);
}

// Returns the velocity of the message.
int MidiMessage::getVelocity() const
{
    return velocity;
}

// Returns the amplitude of the message.
float MidiMessage::getFloatVelocity() const
{
    return velocity / 127.0;
}

// Returns the channel of the message.
int MidiMessage::getChannel() const
{
    return channel;
}